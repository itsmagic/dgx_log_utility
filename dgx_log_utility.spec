# -*- mode: python -*-

block_cipher = None


a = Analysis(['dgx_logs.py'],
             pathex=['C:\\Users\\Jim Maciejewski\\Documents\\dgx_log_utility'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

##### include mydir in distribution #######
def extra_datas(icon):
    def rec_glob(p, files):
        import os
        import glob
        for d in glob.glob(p):
            if os.path.isfile(d):
                files.append(d)
            rec_glob("%s/*" % d, files)
    files = []
    rec_glob("%s/*" % icon, files)
    extra_datas = []
    for f in files:
        extra_datas.append((f, f, 'icon'))

    return extra_datas
############################################
a.datas += extra_datas('dgx_log_utility\icon')
a.datas += extra_datas('dgx_log_utility\doc')


pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='dgx_log_utility_v2.1.2',
          debug=False,
          strip=False,
          upx=True,
          console=False,
          icon='dgx_log_utility\icon\dlu.ico' )

