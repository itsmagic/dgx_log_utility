@echo off
@SETLOCAL ENABLEDELAYEDEXPANSION

ECHO This progarm captures debug data from the DGX Master via telnet to 198.18.0.1
ECHO It also captures debug from the DGX_Shell via telnet to 198.18.128.1
ECHO To do this, establish a network connection with you PC to the ICSLAN port.
SET /p "matrix_size=Next enter the size of the DGX matrix (8,16,32,64): "

FOR /f "tokens=1-8 delims=:./ " %%G IN ("%date%_%time%") DO (
SET datetime=%%H-%%I-%%J"h"%%K"m"
)

start telnet -f DGX_Master_Debug_%datetime%.log 198.18.0.1
cscript Master_SendKeys.vbs %matrix_size%

start telnet -f DGX_Shell_Debug_%datetime%.log 198.18.128.1
cscript Shell_SendKeys.vbs %matrix_size%
