import wx
from dgx_log_utility import dgx_log_controller


def main():
    """Launch the main program"""
    dlu_app = wx.App()
    dlu_frame = dgx_log_controller.MainFrame(None)
    dlu_frame.Show()
    dlu_app.MainLoop()


if __name__ == '__main__':
    main()
