from dgx_log_utility import gather_logs


def test_clean_up():
    data = b' su enova 12amx34\r\r\n\r\r\nPrivilege Level 2\r\r\n\x1b[1GDGX_SHELL>'
    result = gather_logs.GetInfo.clean_up(None, data)
    desr = ' su enova 12amx34\r\r\n\r\r\nPrivilege Level 2\r\r\nDGX_SHELL>'
    assert result == desr

