Description
------------------------------------------------------------------------
 The DGX_LOG_Utility is a simple debug capture tool which automatically 
 gathers debug from the DGX100 Series enclosure. It uses either the private
 DGX ICSLAN port/connection or the public LAN port to connect with the 
 enclosure based on the IP you provide.

------------------------------------------------------------------------
Release v2.0.3
------------------------------------------------------------------------
     * Fixed a race condition with config file
     * Added the ability to set the ports used (this is done in the config file)

Release v2.0.2
------------------------------------------------------------------------
     * Major version change as code has been ported to python 3
     * Adds individual logs for each connection
     * Added count to progress bar that is updated as tasks are discovered
     * Updated shell commands -- ensure you have deleted the local copy of shell_commands.txt to update to the new file
     * Updated error handling to better manage common faults
     * Fixed a process bug where the Get Files could be shown before finished
 
------------------------------------------------------------------------
Setup & Process
------------------------------------------------------------------------
There are two ways to use it: 

1. Connected to the private/internal network (ICSLan)
    * Connect your computer to the ICSLan port or any DXLink device ICSLan. 
        * Wait for your computer to get an DHCP address from the DGX i.e. 198.18.128.XXX 
    * For most the default values should work, just click "Get Logs" and the program 
      will make telnet / ftp connections to download the files. The program will save 
      them and add them to a zip file. 

2. Connected to the public/external network (LAN)
    * In order to connect to the DGX shell the external interface needs to be enabled 
        (Please see Appendix D of the DGX Hardware Reference for details)
    * The basic commands are as follows for a DHCP network 
        * Setup  
        * send_command 5002:1:1, "'IFCONFIG-1, UP, D, hn=<hostname>'" 
          (where hostname is the hostname for the port you are creating...eg. "DGX_Telnet")
        * Confirm 
        * enable notifications on 5002 
        * send_command 5002:1:1, "'?IFCONFIG-1'" 
        * Response: IFCONFIG-1, UP, D, ip=10.103.4.155, gw=10.103.4.1, mask=255.255.255.0, hn=DGX_Telnet 
    * Change the FTP and Shell ip addresses to match the address that the DGX received and was verified 
    * Change the Master ip address to the external IP of the DGX Master 
    * Click "Get Logs" and the and the program will make telnet / ftp connections to download the files.
      The program will save them and add them to a zip file. 

   Notes
    The program will create two command files, master_commands.txt, and shell_commands.txt in the same 
    directory that it is running. If you want to change the commands that are sent, you can edit these 
    files with one command per line and no trailing lines at the end of the file. If you want to reset 
    these files, just delete them and they will be recreated the next time the program is run. 

    The program also creates a configuration file dgx_log_utility.conf This file stores any changes you
    make to the ip addresses or file names. To reset all settings, just delete this file, and the defaults
    will be recreated.

------------------------------------------------------------------------
DGX_LOGS folder contents
------------------------------------------------------------------------
    * LOCALINF.XML --> Current Config file showing mcpu settings
    * Several log files --> DGX log files (33max) stored after each reboot
      * The new option allows you to delete these when done in case they are 
        large and makes the zip file too large each time the utility is run.
    * Activelog.log --> The current DGX logtail capture since last reboot
    * DGX_SHELL_output.log --> which has all the DGX_SHELL debug info
    * MASTER_output.log --> which has all the NX Master debug info

------------------------------------------------------------------------
Known Issues
------------------------------------------------------------------------
If you have a firewall on your network it may not allow this program to 
interact with the ports this program uses to talk to the DGX.

Much of the inter-cage communication in the DGX utilizes the I2C interface.
Commands sent to an I/O board (BCPU) that is not present will result in 
repeated I2C ERROR messages from that BCPU. This is to be expected and 
should not be cause for alarm.

eg.
11:59:27.943 ERROR i2c   : I2C error for GET_LAST_CMD_STATUS on BCPU16
11:59:27.958 ERROR i2c   : I2C read #1/3 failed on BCPU16
11:59:27.977 ERROR i2c   : I2C read #2/3 failed on BCPU16
11:59:27.996 ERROR i2c   : I2C read #3/3 failed on BCPU16



