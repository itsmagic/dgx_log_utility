import serial
from threading import Thread
from pydispatch import dispatcher
import time


class SerialComms(Thread):
    def __init__(self, port_name):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.waiting = False
        dispatcher.connect(self.write, signal="SerialWriter", sender=dispatcher.Any)
        # We need to see if we can get to the port
        try:
            self.ser = serial.Serial(port_name, 9600, timeout=1)
        except Exception as error:
            dispatcher.send(signal='SerialComms', sender=self, message={'error': 'Connecting to port failed: ' + str(error)})
            self.shutdown = True
        Thread.__init__(self)

    def run(self):
        """Run Worker Thread."""
        recv = 0
        while not self.shutdown:
            # print('recv: ', recv)
            # print('waiting: ', self.waiting)
            if self.waiting:
                recv += 1
                # print(recv)
                if recv >= 2:
                    self.waiting = False
                    recv = 0
            # print('we are running')
            # check for input
            data = self.ser.read(1000)
            if data:
                recv = 0
                dispatcher.send(signal='SerialComms', sender=self, message={'incoming': data})
            # check for output
            # time.sleep(5)
            # print('timer done shutting down')
            # self.shutdown = True
        # print('shutdown')
        try:
            self.ser.close()
        except Exception as error:
            pass

    def write(self, message):
        if not self.shutdown:
            self.ser.write(message)
            self.waiting = True


def incoming(message):
    # print('got a message: ', message)
    if 'incoming' in message:
        # print((message['incoming']))
        print(clean_up(message['incoming']))
    else:
        print(message)


def clean_up(data):
        """Removes terminal control chars"""
        # print('clean up: ', data, repr(data))
        data = data.strip(b'\x1b[11G')
        # data = data.strip(b'\x1b[1G')
        data = data.decode()
        output = ""
        data_list = list(data)
        while data_list:
            item = data_list.pop(0)
            if ord(item) == 27:
                data_list.pop(0)
                data_list.pop(0)
                last = data_list.pop(0)
                if last == ";":
                    data_list.pop(0)
                    data_list.pop(0)
                continue
            output += item
        return output


def main():
    print('startup')
    dispatcher.connect(incoming, signal="SerialComms", sender=dispatcher.Any)
    my_mod = SerialComms('COM9')
    my_mod.setDaemon(True)
    my_mod.start()
    time.sleep(1)
    dispatcher.send(signal="SerialWriter", message=b'\x03show\r\n')
    # my_mod.ser.write(b'\x03show\r\n')
    time.sleep(10)
    my_mod.shutdown = True


if __name__ == '__main__':
    main()
