from dgx_log_utility import dgxlogutility_gui
import json
import os
from queue import Queue
from serial.tools import list_ports
import shutil
import subprocess
import sys
import time
import wx
import wx.html

from pydispatch import dispatcher

from dgx_log_utility.gather_logs import FTPLogs, GetInfo, SerialLogs
from dgx_log_utility.update_thread import UpdateThread

MASTER_COMMANDS = ['date', 'time', 'program', 'program info', 'sd', 'url list', 'route mode', 'show notify', 'show remote', 'show buffers', 'show max buffers', 'show mem',
                   'msg stats', 'cpu usage', 'show log all']
SHELL_COMMANDS = ['date', 'su enova 12amx34', 'version', 'cfgvarget 1.1.1.2.3', 'set', 'splash -v3 -i1', 'splash -v3 -i2', 'splash -v3 -i3', 'splash -v3 -i4', 'splash -v3 -i5',
                  'splash -v3 -i6', 'splash -v3 -i7', 'splash -v3 -i9', 'show', 'show aie', 'show stats', 'channel', 'switch 0', 'switch 1', 'switch 2']
SERIAL_COMMANDS = {'shell': ['date', 'version', 'cfgvarget 1.1.1.2.3', 'set', 'show', 'show aie', 'show stats', 'channel', 'switch 0', 'switch 1', 'switch 2'],
                   'bcs': ['~scr1v3!', '~scr2v3!', '~scr3v3!', '~scr4v3!', '~scr5v3!', '~scr6v3!', '~scr7v3!', '~scr9v3!', '~qry!']}

# The MIT License (MIT)

# Copyright (c) 2017 Jim Maciejewski

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


class MainFrame(dgxlogutility_gui.DGXLogUtilityFrame):
    def __init__(self, parent):
        dgxlogutility_gui.DGXLogUtilityFrame.__init__(self, parent)

        self.name = "DGX Log Utility"
        self.version = 'v2.1.2'
        self.SetTitle(self.name + " " + self.version)
        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("dgx_log_utility", "icon", "dlu.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.update_config = False
        self.load_config()
        self.update_config = True
        self.in_legacy_mode = False
        # self.on_toggle_legacy(None)
        self.check_command_files()
        self.get_logs_thread = []
        self.progress_gauge.SetRange(0)
        self.progress_gauge.SetValue(0)
        self.busy = {'ftp': False,
                     'shell': False,
                     'master': False,
                     'started': False}
        self.update_queue = Queue()
        self.update_job_thread = UpdateThread(self, self.update_queue)
        self.update_job_thread.setDaemon(True)
        self.update_job_thread.start()
        # self.serial_cmb.
        dispatcher.connect(self.add_status,
                           signal="Status",
                           sender=dispatcher.Any)
        dispatcher.connect(self.btn_status,
                           signal="Button",
                           sender=dispatcher.Any)
        dispatcher.connect(self.progress_status,
                           signal="Progress",
                           sender=dispatcher.Any)
                           

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def btn_status(self, sender):
        """Disables the button while working"""
        # print('Sender: ', sender)
        for key in sender.keys():
            self.busy[key] = sender[key]
            if self.busy[key]:
                self.busy['started'] = True
                # self.archive_armed = True

        currently_working = self.busy['ftp'] or self.busy['shell'] or self.busy['master']
        # for key in self.busy.keys():
        #     currently_working = currently_working or self.busy[key]
        #     if currently_working:
        #         # Break out here, we are currently working on something
        #         break
        if not currently_working and self.busy['started']:
            if self.archive_chk.GetValue():
                self.archive()
            self.show_files()
            self.busy['started'] = False
        self.get_logs_btn.Enable(not currently_working)  # Only enable the Get Logs button when we are not working
        self.get_serial_logs_btn.Enable(not currently_working)
        # print(self.busy)

    def add_status(self, sender):
        """Add status to status area"""
        self.update_queue.put(['update_status', sender])

    def progress_status(self, sender):
        if 'steps' in sender:
            self.update_queue.put(['update_steps', sender['steps']])
        if 'progress' in sender:
            self.update_queue.put(['update_progress', sender['progress']])

    def clear_status(self):
        self.ftp_status_txt.Clear()
        self.shell_status_txt.Clear()
        self.master_status_txt.Clear()

    def on_toggle_legacy(self, event):
        """Switch between getting logs from a legacy and a dgx"""
        network_items = ['ftp_chk', 'ftp_ip_txt', 'ftp_delete_chk', 'shell_chk', 'shell_command_txt', 'shell_ip_txt', 'shell_output_txt', 'master_chk',
                         'master_ip_txt', 'master_command_txt', 'master_output_txt', 'get_logs_btn']
        serial_items = ['serial_name', 'serial_cmb', 'serial_button', 'serial_name_2', 'serial_command_txt', 'serial_output_txt', 'get_serial_logs_btn']
        if self.in_legacy_mode:
            # show the network stuff
            # print('showing network')
            for item in network_items:
                getattr(self, item).Show()
            for item in serial_items:
                getattr(self, item).Hide()
            self.legacy_menu.Check(False)
        else:
            # show the serial stuff
            # print('showing serial')
            for item in network_items:
                getattr(self, item).Hide()
            for item in serial_items:
                getattr(self, item).Show()
            self.on_update_ports(None)
            self.legacy_menu.Check(True)

        self.Layout()
        self.in_legacy_mode = not self.in_legacy_mode

    def on_update_ports(self, event):
        """Update the list of serial ports"""
        self.serial_cmb.Clear()
        ports = list_ports.comports()
        for port in reversed(ports):
            self.serial_cmb.Append(port.device)
            self.serial_cmb.SetSelection(0)

    def on_get_serial_logs(self, event):
        """Get the serial logs"""
        # print('in get serial logs ', self.serial_cmb.GetValue())
        port = self.serial_cmb.GetValue()
        if port != 'None':
            self.folder_name = "DGX_LOGS_" + time.strftime("%Y%m%d-%H%M%S")
            self.clear_status()
            self.steps = 0
            self.progress_gauge.SetValue(0)
            self.com_steps_txt.SetLabel("0")
            self.tot_steps_txt.SetLabel("0")
            my_serial = SerialLogs(command_file=self.serial_command_txt.GetValue(),
                                   port=port,
                                   folder_name=self.folder_name,
                                   output_file=self.serial_output_txt.GetValue())
            my_serial.setDaemon = True
            my_serial.start()

    def on_get_logs(self, event):
        """Put this job in a thread so we don't hang the main window"""
        """Figure out what is ticked and get it"""
        if self.ftp_delete_chk.GetValue():
            # We need to delete DGX logs
            dlg = wx.MessageDialog(self,
                                   message='Warning this will download and then delete all DGX logs!\r',
                                   caption='Delete Logs',
                                   style=wx.ICON_INFORMATION | wx.OK | wx.CANCEL)
            if dlg.ShowModal() != wx.ID_OK:
                return

        self.folder_name = "DGX_LOGS_" + time.strftime("%Y%m%d-%H%M%S")
        self.clear_status()
        self.steps = 0
        self.progress_gauge.SetValue(0)
        self.com_steps_txt.SetLabel("0")
        self.tot_steps_txt.SetLabel("0")
        if self.ftp_chk.GetValue():
            my_ftp = FTPLogs(ip_address=self.ftp_ip_txt.GetValue(),
                             port_number=self.ftp_port_txt.GetValue(),
                             dgx_master_port_number=self.shell_port_txt.GetValue(),
                             folder_name=self.folder_name,
                             delete=self.ftp_delete_chk.GetValue())
            my_ftp.setDaemon = True
            my_ftp.start()

        if self.shell_chk.GetValue():
            my_shell = GetInfo(ip_address=self.shell_ip_txt.GetValue(),
                               port_number=self.shell_port_txt.GetValue(),
                               script_name="shell",
                               command_file=self.shell_command_txt.GetValue(),
                               output_file=self.shell_output_txt.GetValue(),
                               folder_name=self.folder_name,
                               username=self.username_txt.GetValue(),
                               password=self.password_txt.GetValue())
            my_shell.setDaemon = True
            my_shell.start()
        if self.master_chk.GetValue():
            my_master = GetInfo(ip_address=self.master_ip_txt.GetValue(),
                                port_number=self.master_port_txt.GetValue(),
                                script_name="master",
                                command_file=self.master_command_txt.GetValue(),
                                output_file=self.master_output_txt.GetValue(),
                                folder_name=self.folder_name,
                                username=self.username_txt.GetValue(),
                                password=self.password_txt.GetValue())
            my_master.setDaemon = True
            my_master.start()

    def archive(self):
        if os.path.exists(self.folder_name):
            # dispatcher.send(signal="Status", sender='Archiving Log Folder')
            shutil.make_archive(self.folder_name, 'zip', self.folder_name)
            # dispatcher.send(signal="Status", sender='All done')
            # self.progress_gauge.SetValue(0)
            # self.progress_gauge.SetRange(0)

    def show_files(self):
        if os.path.exists(self.folder_name):
            cmd = r'explorer "{}"'.format(self.folder_name)
            subprocess.Popen(cmd)

    def on_ftp_delete(self, event):
        """Check FTP delete"""
        self.delete_toggle(not self.ftp_delete_chk.GetValue())

    def delete_toggle(self, enabled):
        """Show or hide buttons for delete"""
        if enabled:
            self.get_logs_btn.SetLabel("Get Logs")
            self.Fit()
        else:
            self.get_logs_btn.SetLabel("Delete DGX Logs")
            self.Fit()

    def on_quit(self, event):
        """Close out the program"""
        self.Destroy()

    def check_command_files(self):
        """Verifys the command files exsist and creates them if not"""
        if not os.path.exists('master_commands.txt'):
            with open('master_commands.txt', 'w') as f:
                for item in MASTER_COMMANDS:
                    f.write(item + '\r')

        if not os.path.exists('shell_commands.txt'):
            with open('shell_commands.txt', 'w') as f:
                for item in SHELL_COMMANDS:
                    f.write(item + '\r')

        if not os.path.exists('serial_commands.txt'):
            with open('serial_commands.txt', 'w') as f:
                json.dump(SERIAL_COMMANDS, f)

    def load_config(self):
        """Loads the saved config"""
        try:
            with open('dgx_log_utility.conf', 'r') as f:
                config = json.load(f)
            if "username" in config:
                self.username_txt.SetValue(config['username'])
            if "password" in config:
                self.password_txt.SetValue(config['password'])
            if "ftp" in config:
                self.ftp_chk.SetValue(config['ftp']['chk'])
                self.ftp_ip_txt.SetValue(config['ftp']['ip'])
                self.ftp_port_txt.SetValue(config['ftp']['port'])
            if "shell" in config:
                self.shell_chk.SetValue(config['shell']['chk'])
                self.shell_ip_txt.SetValue(config['shell']['ip'])
                self.shell_command_txt.SetValue(config['shell']['command'])
                self.shell_output_txt.SetValue(config['shell']['output'])
                self.shell_port_txt.SetValue(config['shell']['port'])
            if "master" in config:
                self.master_chk.SetValue(config['master']['chk'])
                self.master_ip_txt.SetValue(config['master']['ip'])
                self.master_command_txt.SetValue(config['master']['command'])
                self.master_output_txt.SetValue(config['master']['output'])
                self.master_port_txt.SetValue(config['master']['port'])
            if "archive" in config:
                self.archive_chk.SetValue(config['archive'])

        except Exception as error:
            print("unable to load configuration: ", error)

    def on_save_config(self, event):
        """Catches any changes to the config and saves them"""
        if not self.update_config:
            return
        self.save_config()

    def save_config(self):
        """Save the ip addresses for next time"""
        try:
            with open('dgx_log_utility.conf', 'w') as f:
                config = {"username": self.username_txt.GetValue(),
                          "password": self.password_txt.GetValue(),
                          "ftp": {"chk": self.ftp_chk.GetValue(),
                                  "ip": self.ftp_ip_txt.GetValue(),
                                  "port": self.ftp_port_txt.GetValue()},
                          "shell": {"chk": self.shell_chk.GetValue(),
                                    "ip": self.shell_ip_txt.GetValue(),
                                    "command": self.shell_command_txt.GetValue(),
                                    "output": self.shell_output_txt.GetValue(),
                                    "port": self.shell_port_txt.GetValue()},
                          "master": {"chk": self.master_chk.GetValue(),
                                     "ip": self.master_ip_txt.GetValue(),
                                     "command": self.master_command_txt.GetValue(),
                                     "output": self.master_output_txt.GetValue(),
                                     "port": self.master_port_txt.GetValue()},
                          "archive": self.archive_chk.GetValue()
                          }
                # f.write(json.dumps(config))
                json.dump(config, f)
        except Exception as error:
            print("unable to save configuration: ", error)

    def on_reset_config(self, event):
        """Resets the Username, Password, IP Addresses, and filenames"""
        self.username_txt.SetValue("administrator")
        self.password_txt.SetValue("password")
        self.ftp_chk.SetValue(True)
        self.ftp_ip_txt.SetValue("198.18.128.1")
        self.ftp_port_txt.SetValue("21")
        self.shell_chk.SetValue(True)
        self.shell_ip_txt.SetValue("198.18.128.1")
        self.shell_command_txt.SetValue("shell_commands.txt")
        self.shell_output_txt.SetValue("DGX_SHELL_output.log")
        self.shell_port_txt.SetValue("23")
        self.master_chk.SetValue(True)
        self.master_ip_txt.SetValue("198.18.0.1")
        self.master_command_txt.SetValue("master_commands.txt")
        self.master_output_txt.SetValue("MASTER_output.log")
        self.master_port_txt.SetValue("23")
        self.archive_chk.SetValue(True)

    def on_documentation(self, event):
        aboutDlg = dgxlogutility_gui.AboutFrame(None)
        html = wx.html.HtmlWindow(aboutDlg)
        with open(self.resource_path(os.path.join('dgx_log_utility', 'doc', 'documentation.html')), 'r') as f:
            html.SetPage(f.read())
        aboutDlg.Show()


