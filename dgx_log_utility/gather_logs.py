import json
import os
import telnetlib
import time
from threading import Thread
from ftplib import FTP
from pydispatch import dispatcher
from dgx_log_utility.serial_comms import SerialComms

# Added this so we get the correct error when telnet fails (only happens after being built)
import encodings.idna


class GetInfo(Thread):
    def __init__(self, ip_address, port_number, script_name, command_file, folder_name, output_file, username="administrator", password="password"):
        self.ip_address = ip_address
        self.port_number = port_number
        self.script_name = script_name
        self.command_file = command_file
        self.folder_name = folder_name
        self.output_file = output_file
        self.username = username
        self.password = password
        dispatcher.send(signal="Button", sender={self.script_name: True})
        Thread.__init__(self)

    def run(self):
        """Gathers info from DGX"""
        self.send_status(["Connecting to telnet: " + self.ip_address + ':' + self.port_number, self.script_name])
        try:
            telnet_session = self.establish_telnet(self.ip_address, int(self.port_number))
            need_to_login, intro = self.check_for_login(telnet_session)
            if need_to_login:
                intro = self.login(telnet_session,
                                   self.username,
                                   self.password)
                if intro is False:
                    self.send_status(['Login Failed', self.script_name])
                    dispatcher.send(signal="Button", sender={self.script_name: False})
                    return
            if intro[-1] != b'>':
                intro += telnet_session.read_until(b'>', 5).split()
            telnet_session.write(b'\r\n')
            telnet_session.read_until(b'>')

            self.send_status(["Connection Successful", self.script_name])
        except Exception as error:
            dispatcher.send(signal="Button", sender={self.script_name: False})
            self.send_status(['Connection error: ' + str(error), self.script_name])
            return
        # try:
        # at this point we have a prompt and are ready to go #
        # lets read a file with one command per line #
        command_list = []
        with open(self.command_file, 'r') as f:
            command_list = f.readlines()

        command_list = [x.strip() for x in command_list]

        output = ''
        self.send_progress({'steps': len(command_list)})
        for command in command_list:
            self.send_status(["Sending command: " + command, self.script_name])
            telnet_session.write(command.encode() + b'\r\n')
            if self.script_name == "shell":
                response = telnet_session.read_until(b'SHELL>')  # need SHELL> or else '>' in some shell debug will cause issues
            else:
                response = telnet_session.read_until(b'\n>')   # need '\n>' or else '>' in some master debug will cause issues
            output += self.clean_up(response)
            self.send_progress({'progress': 1})

        telnet_session.close()
        self.create_output_folder()
        with open(os.path.join(self.folder_name, self.output_file), 'w') as f:
            f.write(output)
        # except Exception as error:
        #     self.send_status(['Sending command error: ' + str(error), self.script_name])
        self.send_status([self.script_name + ' Done', self.script_name])
        dispatcher.send(signal="Button", sender={self.script_name: False})

    def create_output_folder(self):
        if not os.path.exists(self.folder_name):
            os.makedirs(self.folder_name)

    def establish_telnet(self, ip_address, tel_port=23):
        """Creates the telnet instance"""
        telnet_session = telnetlib.Telnet(ip_address, tel_port, 5)
        telnet_session.set_option_negotiation_callback(self.call_back)
        return telnet_session

    def call_back(self, sock, cmd, opt):
        """ Turns on server side echoing"""
        if opt == telnetlib.ECHO and cmd in (telnetlib.WILL, telnetlib.WONT):
            sock.sendall(telnetlib.IAC + telnetlib.DO + telnetlib.ECHO)

    def check_for_login(self, telnet_session):
        """Checks if we need to login and returns the header"""
        intro = telnet_session.read_until(b'>', .5)
        if intro.split()[0] == b'Login':
            need_to_login = True
        else:
            need_to_login = False
        return need_to_login, intro.split()

    def login(self, telnet_session, username, password):
        """Log in when required"""
        try:
            telnet_session.write(username.encode() + b'\r')
            telnet_session.read_until(b'Password :', 5)
            telnet_session.write(password.encode() + b'\r')
            # have to read_until, will timeout if password is wrong
            intro = telnet_session.read_until(b'>', 5).split()[1:]
            # print 'intro ', intro
            if intro[0] != b'Welcome':
                return False
            else:
                return intro
        except Exception as error:
            self.send_status(['during login: ' + str(error), self.script_name])
            return False

    def clean_up(self, data):
        """Removes terminal control chars"""
        # print('clean up: ', data, repr(data))
        data = data.decode()
        output = ""
        data_list = list(data)
        while data_list:
            item = data_list.pop(0)
            if ord(item) == 27:
                data_list.pop(0)
                data_list.pop(0)
                last = data_list.pop(0)
                if last == ";":
                    data_list.pop(0)
                    data_list.pop(0)
                continue
            output += item
        return output

    def send_progress(self, progress):
        dispatcher.send(signal="Progress", sender=progress)

    def send_status(self, status):
        """Updates status"""
        dispatcher.send(signal="Status", sender=status)


class FTPLogs(Thread):
    def __init__(self, ip_address, port_number, dgx_master_port_number, folder_name, delete=False):
        self.ip_address = ip_address
        self.port_number = port_number
        self.dgx_master_port_number = dgx_master_port_number
        self.folder_name = folder_name
        self.delete = delete
        dispatcher.send(signal="Button", sender={'ftp': True})
        Thread.__init__(self)

    def create_output_folder(self):
        if not os.path.exists(self.folder_name):
            os.makedirs(self.folder_name)

    def establish_telnet(self, ip_address, tel_port=23):
        """Creates the telnet instance"""
        telnet_session = telnetlib.Telnet(ip_address, tel_port, 5)
        telnet_session.set_option_negotiation_callback(self.call_back)
        return telnet_session

    def call_back(self, sock, cmd, opt):
        """ Turns on server side echoing"""
        if opt == telnetlib.ECHO and cmd in (telnetlib.WILL, telnetlib.WONT):
            sock.sendall(telnetlib.IAC + telnetlib.DO + telnetlib.ECHO)

    def run(self):
        """Collects all the log files via FTP """

        # First we need to Verify that they have a valid ICSLAN connection to the PC
        # self.send_status(["Verifying ICSLAN Port Connection to PC")
        """Could probably do a simple ping here...or else make the exception case say there is no connection"""

        # First we need to export the active log
        self.send_status(["Connecting to telnet to export active log: " + self.ip_address + ':' + self.dgx_master_port_number, 'ftp'])
        try:
            telnet_session = self.establish_telnet(self.ip_address, int(self.dgx_master_port_number))
            telnet_session.write(b'\r\n')
            telnet_session.read_until(b'>')
            self.send_status(["Connection Successful", 'ftp'])
            self.send_status(["Exporting Active Logtail", 'ftp'])
            telnet_session.write(b'log export active log\\ActiveLogtail.log\r\n')
            telnet_session.read_until(b'>')
            telnet_session.close()

        except Exception as error:
            self.send_status(['Connection error: ' + str(error), 'ftp'])

        self.send_status(["Connecting to ftp: " + self.ip_address + ':' + self.port_number, 'ftp'])

        try:
            ftp = FTP(self.ip_address, timeout=5)
            ftp.login()  # Logs in as anonymous
            ftp.sendcmd('cd CONFIG')
            self.create_output_folder()
            output = ftp.retrbinary('RETR ' + 'LOCALINF.XML', open(os.path.join(self.folder_name, 'LOCALINF.XML'), 'wb').write)
            self.send_status([output, 'ftp'])
            ftp.sendcmd('cd ..\LOG')
            file_list = ftp.nlst()
            self.send_progress({"steps": len(file_list[2:])})
            for item in file_list[2:]:
                self.send_progress({'progress': 1})
                try:
                    self.create_output_folder()
                    output = ftp.retrbinary('RETR ' + item, open(os.path.join(self.folder_name, item), 'wb').write)
                    if self.delete:
                        ftp.delete(os.path.join(item))
                    self.send_status([output, 'ftp'])
                except Exception as error:
                    print("Skipping: ", item, " due to error: ", error)
            ftp.quit()
        except Exception as error:
            self.send_status(['Connection error: ' + str(error), 'ftp'])
            dispatcher.send(signal="Button", sender={'ftp': False})
            return
        self.send_status(['FTP Done', 'ftp'])
        dispatcher.send(signal="Button", sender={'ftp': False})

    def send_progress(self, progress):
        dispatcher.send(signal="Progress", sender=progress)

    def send_status(self, status):
        """Updates status"""
        dispatcher.send(signal="Status", sender=status)


class SerialLogs(Thread):
    def __init__(self, command_file, port, folder_name, output_file):
        self.command_file = command_file
        self.port = port
        self.folder_name = folder_name
        self.output_file = output_file
        self.output = ''
        self.output_current_length = -1
        dispatcher.connect(self.incoming, signal="SerialComms", sender=dispatcher.Any)
        dispatcher.send(signal="Button", sender={'shell': True})
        Thread.__init__(self)

    def create_output_folder(self):
        if not os.path.exists(self.folder_name):
            os.makedirs(self.folder_name)

    def run(self):
        """Gathers info from a DGX via serial """
        # connect to serial port and ensure it is valid...
        all_commands = []
        try:
            with open(self.command_file, 'r') as f:
                all_commands = json.load(f)
        except Exception as error:
            message = 'Unable to read ' + self.command_file + ' please delete it and restart the program. The error was: ' + str(error)
            self.send_status([message, 'shell'])
            dispatcher.send(signal="Button", sender={'shell': False})
            return
        bcs_commands = all_commands['bcs']
        shell_commands = all_commands['shell']
        self.send_progress({"steps": len(bcs_commands) + len(shell_commands)})
        self.serial_comms = SerialComms(self.port)
        self.serial_comms.setDaemon(True)
        self.serial_comms.start()
        command_sequence = ['\x03'] + shell_commands + ['bcs'] + bcs_commands
        if not self.serial_comms.shutdown:
            for item in command_sequence:
                while self.serial_comms.waiting:
                    time.sleep(1)
                if len(self.output) == self.output_current_length:
                    self.send_status(["Warning: No response received!", 'shell'])
                self.output_current_length = len(self.output)
                self.serial_comms.write((item + '\r').encode())
                if item not in ['\x03', 'bcs']:
                    self.send_status(["Sending command: " + item, 'shell'])
                    self.send_progress({'progress': 1})
                elif item == '\x03':
                    self.send_status(["Sending command: ctrl+c", 'shell'])
                elif item == 'bcs':
                    self.send_status(["Sending command: bcs", 'shell'])

            self.create_output_folder()
            while self.serial_comms.waiting:
                time.sleep(1)
            with open(os.path.join(self.folder_name, self.output_file), 'w') as f:
                f.write(self.output)
            self.send_status(['Serial Done', 'shell'])
        self.serial_comms.shutdown = True
        dispatcher.send(signal="Button", sender={'shell': False})

    def incoming(self, message):
        if 'incoming' in message:
            # print((message['incoming']))
            self.output += self.clean_up(message['incoming'])
        if 'error' in message:
            self.send_status([message['error'], 'shell'])
        # else:
        #     print(message)

    def clean_up(self, data):
        """Removes terminal control chars"""
        # print('clean up: ', data, repr(data))
        data = data.strip(b'\x1b[11G')
        # data = data.strip(b'\x1b[1G')
        data = data.decode()
        output = ""
        data_list = list(data)
        while data_list:
            item = data_list.pop(0)
            if ord(item) == 27:
                data_list.pop(0)
                data_list.pop(0)
                last = data_list.pop(0)
                if last == ";":
                    data_list.pop(0)
                    data_list.pop(0)
                continue
            output += item
        return output

    def send_progress(self, progress):
        dispatcher.send(signal="Progress", sender=progress)

    def send_status(self, status):
        """Updates status"""
        dispatcher.send(signal="Status", sender=status)


def show_status(sender):
    print(sender)


def main():
    dispatcher.connect(show_status,
                       signal="Status",
                       sender=dispatcher.Any)
    folder_name = "DGX_LOGS_TEST"
    GetInfo(ip_address="198.18.0.1",
            script_name="master",
            command_file="master_commands.txt",
            folder_name=folder_name,
            output_file="output_test.txt").start()
    input()


if __name__ == '__main__':
    main()
