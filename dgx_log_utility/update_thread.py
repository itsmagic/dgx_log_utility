from threading import Thread
import datetime


class UpdateThread(Thread):

    def __init__(self, parent, queue):
        Thread.__init__(self)
        self.queue = queue
        self.parent = parent

    def run(self):
        while True:
            # gets the job from the queue
            job = self.queue.get()
            getattr(self, job[0])(job)

            # self.olv_queue.put(['add_object', data])

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def update_status(self, job):
        # print "job: ", job
        datetime_stamp = datetime.datetime.now().strftime("%H:%M:%S.%f")[:-3]
        update_txt = datetime_stamp + " >> " + job[1][0] + "\n"
        field = job[1][1]
        if field == 'ftp':
            self.parent.ftp_status_txt.AppendText(update_txt)
        elif field == 'shell':
            self.parent.shell_status_txt.AppendText(update_txt)
        elif field == 'master':
            self.parent.master_status_txt.AppendText(update_txt)
        else:
            print('Error, I dont know field: ', field, job)

    def update_steps(self, job):
        steps = job[1]
        self.parent.steps += steps
        self.parent.progress_gauge.SetRange(self.parent.steps)
        self.parent.tot_steps_txt.SetLabel(str(self.parent.steps))

    def update_progress(self, job):
        progress = job[1]
        new_count = self.parent.progress_gauge.GetValue() + progress
        self.parent.progress_gauge.SetValue(new_count)
        self.parent.com_steps_txt.SetLabel(str(new_count))

