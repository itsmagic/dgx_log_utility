# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Aug  8 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class DGXLogUtilityFrame
###########################################################################

class DGXLogUtilityFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"DGX Log Utility", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.RESIZE_BORDER|wx.TAB_TRAVERSAL )
		
		self.SetSizeHints( wx.Size( 500,700 ), wx.DefaultSize )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer3 = wx.BoxSizer( wx.VERTICAL )
		
		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Settings" ), wx.HORIZONTAL )
		
		bSizer9 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText1 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Username", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )
		
		self.m_staticText1.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer9.Add( self.m_staticText1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.username_txt = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, u"administrator", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer9.Add( self.username_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		sbSizer2.Add( bSizer9, 1, wx.EXPAND, 5 )
		
		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText2 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )
		
		self.m_staticText2.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer10.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.password_txt = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, u"password", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer10.Add( self.password_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		sbSizer2.Add( bSizer10, 1, wx.EXPAND, 5 )
		
		
		bSizer3.Add( sbSizer2, 0, wx.EXPAND|wx.ALL, 5 )
		
		bSizer15 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.serial_name = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Serial Port", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.serial_name.Wrap( -1 )
		
		self.serial_name.Hide()
		
		bSizer15.Add( self.serial_name, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		serial_cmbChoices = []
		self.serial_cmb = wx.ComboBox( self.m_panel1, wx.ID_ANY, u"None", wx.DefaultPosition, wx.DefaultSize, serial_cmbChoices, 0 )
		self.serial_cmb.Hide()
		
		bSizer15.Add( self.serial_cmb, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.serial_button = wx.Button( self.m_panel1, wx.ID_ANY, u"Update Ports", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.serial_button.Hide()
		
		bSizer15.Add( self.serial_button, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer3.Add( bSizer15, 0, wx.EXPAND|wx.ALL, 5 )
		
		bSizer17 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.serial_name_2 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Serial Files", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.serial_name_2.Wrap( -1 )
		
		self.serial_name_2.Hide()
		
		bSizer17.Add( self.serial_name_2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.serial_command_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"serial_commands.txt", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.serial_command_txt.Hide()
		
		bSizer17.Add( self.serial_command_txt, 1, wx.ALL, 5 )
		
		self.serial_output_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"SERIAL_output.log", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.serial_output_txt.Hide()
		
		bSizer17.Add( self.serial_output_txt, 1, wx.ALL, 5 )
		
		self.get_serial_logs_btn = wx.Button( self.m_panel1, wx.ID_ANY, u"Get Logs", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.get_serial_logs_btn.Hide()
		
		bSizer17.Add( self.get_serial_logs_btn, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer3.Add( bSizer17, 0, wx.EXPAND|wx.ALL, 5 )
		
		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.ftp_chk = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"FTP", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ftp_chk.SetValue(True) 
		self.ftp_chk.SetToolTip( u"Gather FTP logs" )
		self.ftp_chk.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer4.Add( self.ftp_chk, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.ftp_ip_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"198.18.128.1", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.ftp_ip_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.ftp_delete_chk = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Delete DGX Log Files", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ftp_delete_chk.SetToolTip( u"Check this box to remove all log files from DGX" )
		
		bSizer4.Add( self.ftp_delete_chk, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.ftp_port_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"21", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ftp_port_txt.Hide()
		
		bSizer4.Add( self.ftp_port_txt, 0, wx.ALL, 5 )
		
		
		bSizer3.Add( bSizer4, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		bSizer5 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.shell_chk = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Shell", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.shell_chk.SetValue(True) 
		self.shell_chk.SetToolTip( u"Gather Shell Logs" )
		self.shell_chk.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer5.Add( self.shell_chk, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.shell_ip_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"198.18.128.1", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer5.Add( self.shell_ip_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.shell_command_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"shell_commands.txt", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer5.Add( self.shell_command_txt, 1, wx.ALL, 5 )
		
		self.shell_output_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"DGX_SHELL_output.log", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer5.Add( self.shell_output_txt, 1, wx.ALL, 5 )
		
		self.shell_port_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"23", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.shell_port_txt.Hide()
		
		bSizer5.Add( self.shell_port_txt, 0, wx.ALL, 5 )
		
		
		bSizer3.Add( bSizer5, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		bSizer7 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.master_chk = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Master", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.master_chk.SetValue(True) 
		self.master_chk.SetToolTip( u"Gather Master logs" )
		self.master_chk.SetMinSize( wx.Size( 60,-1 ) )
		
		bSizer7.Add( self.master_chk, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.master_ip_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"198.18.0.1", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer7.Add( self.master_ip_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.master_command_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"master_commands.txt", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer7.Add( self.master_command_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.master_output_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"MASTER_output.log", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer7.Add( self.master_output_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.master_port_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"23", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.master_port_txt.Hide()
		
		bSizer7.Add( self.master_port_txt, 0, wx.ALL, 5 )
		
		
		bSizer3.Add( bSizer7, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		bSizer81 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.archive_chk = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Archive", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.archive_chk.SetValue(True) 
		self.archive_chk.SetToolTip( u"Zip all files in to a single archive" )
		
		bSizer81.Add( self.archive_chk, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		bSizer11 = wx.BoxSizer( wx.VERTICAL )
		
		self.get_logs_btn = wx.Button( self.m_panel1, wx.ID_ANY, u"Get Logs", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer11.Add( self.get_logs_btn, 1, wx.ALL|wx.ALIGN_RIGHT, 5 )
		
		
		bSizer81.Add( bSizer11, 0, 0, 5 )
		
		
		bSizer3.Add( bSizer81, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		bSizer8 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer12 = wx.BoxSizer( wx.HORIZONTAL )
		
		sbSizer51 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Progress" ), wx.HORIZONTAL )
		
		self.progress_gauge = wx.Gauge( sbSizer51.GetStaticBox(), wx.ID_ANY, 100, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL )
		self.progress_gauge.SetValue( 0 ) 
		sbSizer51.Add( self.progress_gauge, 1, wx.ALL, 5 )
		
		bSizer13 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.com_steps_txt = wx.StaticText( sbSizer51.GetStaticBox(), wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.com_steps_txt.Wrap( -1 )
		
		self.com_steps_txt.SetMinSize( wx.Size( 15,-1 ) )
		
		bSizer13.Add( self.com_steps_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		sbSizer51.Add( bSizer13, 0, 0, 5 )
		
		self.m_staticText5 = wx.StaticText( sbSizer51.GetStaticBox(), wx.ID_ANY, u"of", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		
		self.m_staticText5.SetMinSize( wx.Size( 15,-1 ) )
		
		sbSizer51.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		bSizer14 = wx.BoxSizer( wx.VERTICAL )
		
		self.tot_steps_txt = wx.StaticText( sbSizer51.GetStaticBox(), wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.tot_steps_txt.Wrap( -1 )
		
		self.tot_steps_txt.SetMinSize( wx.Size( 15,-1 ) )
		
		bSizer14.Add( self.tot_steps_txt, 0, wx.ALL, 5 )
		
		
		sbSizer51.Add( bSizer14, 0, 0, 5 )
		
		
		bSizer12.Add( sbSizer51, 1, wx.EXPAND, 5 )
		
		
		bSizer8.Add( bSizer12, 1, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		bSizer3.Add( bSizer8, 0, wx.EXPAND|wx.RIGHT|wx.LEFT, 5 )
		
		sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"FTP Status" ), wx.VERTICAL )
		
		self.ftp_status_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_WORDWRAP )
		self.ftp_status_txt.SetMinSize( wx.Size( -1,100 ) )
		
		sbSizer1.Add( self.ftp_status_txt, 1, wx.EXPAND, 5 )
		
		
		bSizer3.Add( sbSizer1, 1, wx.EXPAND|wx.ALL, 5 )
		
		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Shell Status" ), wx.VERTICAL )
		
		self.shell_status_txt = wx.TextCtrl( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_WORDWRAP )
		self.shell_status_txt.SetMinSize( wx.Size( -1,100 ) )
		
		sbSizer3.Add( self.shell_status_txt, 1, wx.EXPAND, 5 )
		
		
		bSizer3.Add( sbSizer3, 1, wx.EXPAND|wx.ALL, 5 )
		
		sbSizer5 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Master Status" ), wx.VERTICAL )
		
		self.master_status_txt = wx.TextCtrl( sbSizer5.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_WORDWRAP )
		self.master_status_txt.SetMinSize( wx.Size( -1,100 ) )
		
		sbSizer5.Add( self.master_status_txt, 1, wx.EXPAND, 5 )
		
		
		bSizer3.Add( sbSizer5, 1, wx.EXPAND|wx.ALL, 5 )
		
		
		self.m_panel1.SetSizer( bSizer3 )
		self.m_panel1.Layout()
		bSizer3.Fit( self.m_panel1 )
		bSizer2.Add( self.m_panel1, 1, wx.EXPAND, 5 )
		
		
		bSizer1.Add( bSizer2, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		bSizer1.Fit( self )
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.legacy_menu = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Serial Debug", wx.EmptyString, wx.ITEM_CHECK )
		self.m_menu1.Append( self.legacy_menu )
		
		self.reset_config = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Reset config", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.reset_config )
		
		self.quit_menu = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Quit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.quit_menu )
		
		self.m_menubar1.Append( self.m_menu1, u"File" ) 
		
		self.m_menu2 = wx.Menu()
		self.m_menuItem3 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Help", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.m_menuItem3 )
		
		self.m_menubar1.Append( self.m_menu2, u"About" ) 
		
		self.SetMenuBar( self.m_menubar1 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.username_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.password_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.serial_button.Bind( wx.EVT_BUTTON, self.on_update_ports )
		self.get_serial_logs_btn.Bind( wx.EVT_BUTTON, self.on_get_serial_logs )
		self.ftp_chk.Bind( wx.EVT_CHECKBOX, self.on_save_config )
		self.ftp_ip_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.ftp_delete_chk.Bind( wx.EVT_CHECKBOX, self.on_ftp_delete )
		self.shell_chk.Bind( wx.EVT_CHECKBOX, self.on_save_config )
		self.shell_ip_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.shell_command_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.shell_output_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.master_chk.Bind( wx.EVT_CHECKBOX, self.on_save_config )
		self.master_ip_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.master_command_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.master_output_txt.Bind( wx.EVT_TEXT, self.on_save_config )
		self.archive_chk.Bind( wx.EVT_CHECKBOX, self.on_save_config )
		self.get_logs_btn.Bind( wx.EVT_BUTTON, self.on_get_logs )
		self.Bind( wx.EVT_MENU, self.on_toggle_legacy, id = self.legacy_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_reset_config, id = self.reset_config.GetId() )
		self.Bind( wx.EVT_MENU, self.on_quit, id = self.quit_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_documentation, id = self.m_menuItem3.GetId() )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def on_save_config( self, event ):
		event.Skip()
	
	
	def on_update_ports( self, event ):
		event.Skip()
	
	def on_get_serial_logs( self, event ):
		event.Skip()
	
	
	
	def on_ftp_delete( self, event ):
		event.Skip()
	
	
	
	
	
	
	
	
	
	
	def on_get_logs( self, event ):
		event.Skip()
	
	def on_toggle_legacy( self, event ):
		event.Skip()
	
	def on_reset_config( self, event ):
		event.Skip()
	
	def on_quit( self, event ):
		event.Skip()
	
	def on_documentation( self, event ):
		event.Skip()
	

###########################################################################
## Class AboutFrame
###########################################################################

class AboutFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 800,700 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	

